from django.urls import path
from .views import search_book, search_book_api
#url for app
urlpatterns = [
    path('', search_book, name='search-book'),
    path('<str:title>/', search_book_api)
]