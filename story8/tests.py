from django.test import TestCase, Client
from django.urls import resolve
from .views import search_book, search_book_api

# Create your tests here.
class Story8Test(TestCase):
    # Checking URLS
    def test_searching_url_exist(self):
        response = Client().get('/story-8/')
        self.assertEqual(response.status_code,200)

    def test_searching_api_exist(self):
        response = Client().get('/story-8/Persona%204/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_searching_views_exist(self):
        found = resolve('/story-8/')
        self.assertEqual(found.func, search_book)

    def test_searching_api_views_exist(self):
        found = resolve('/story-8/Persona%204/')
        self.assertEqual(found.func, search_book_api)

    # Checking Templates
    def test_searching_url_templates_exist(self):
        response = Client().get('/story-8/')
        self.assertTemplateUsed(response,'search_book.html')