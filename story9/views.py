from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import RegisterForm

# Create your views here.
def register_view(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"Account Created for {username}")
            return redirect('login')
    else:
        form = RegisterForm()
    context = {'form':form}
    return render(request,'register.html',context)

def welcome_view(request):
    return render(request, 'welcome.html')