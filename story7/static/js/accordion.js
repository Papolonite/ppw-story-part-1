$(".expand").click(function() {
    var class_select_content = $(this).parents(".accordion").find(".accordion-content"); //For Customizing Content
    var class_select_title = $(this).parents(".accordion-title") // For Customizing Title 

    if (class_select_content.css("overflow") == "hidden") {
        // Changing Display Type
        class_select_content.css("overflow","auto");
        class_select_content.css("height","100%");
        class_select_content.css("padding","2%")

        // Changing Border properties
        class_select_title.css("border-bottom-left-radius","0px");
        class_select_title.css("border-bottom-right-radius","0px");

        // Changing Icon
        setTimeout(function(){},100);
        class_select_title.find(".change").attr("src","/static/svg/minus.svg");


    } else if (class_select_content.css("overflow") == "auto") {
        // Changing Display Type
        class_select_content.css("overflow","hidden");
        class_select_content.css("height","0");
        class_select_content.css("padding","0")

        // Changing Border properties
        class_select_title.css("border-bottom-left-radius","15px");
        class_select_title.css("border-bottom-right-radius","15px");

        // Changing Icon
        setTimeout(function(){},200);
        class_select_title.find(".change").attr("src","/static/svg/plus.svg");
    }
})

$(".down").click(function() {
    var class_select_content = $(this).parent().parent();
    class_select_content.next().insertBefore(class_select_content);
})

$(".up").click(function() {
    var class_select_content = $(this).parent().parent();
    class_select_content.prev().insertAfter(class_select_content);
})

