from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
def accordion_views(request):
    return render(request,'accordion.html')