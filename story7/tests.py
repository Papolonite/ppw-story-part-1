from django.test import TestCase, Client
from django.urls import resolve
from .views import accordion_views

# Create your tests here.
class Story7Test(TestCase):
    # Checking URLS
    def test_accordion_url_exist(self):
        response = Client().get('/story-7/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_accordion_views_exist(self):
        found = resolve('/story-7/')
        self.assertEqual(found.func, accordion_views)

    # Checking Templates
    def test_accordion_templates_exist(self):
        response = Client().get('/story-7/')
        self.assertTemplateUsed(response,'accordion.html')