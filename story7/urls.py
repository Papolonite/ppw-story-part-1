from django.urls import path
from .views import accordion_views
#url for app
urlpatterns = [
    path('', accordion_views, name='accordion'),
]